import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  control = new FormControl(5);
  options = [
    { label: 'Freie Verwendung', value: 8 },
    { label: 'Freie', value: 7 },
    { label: 'Gebrauchtfahrzeug', value: 6 },
    { label: 'Neufahrzeug', value: 5 },
    { label: 'Motorrad', value: 4 },
    { label: 'Umschuldung/Kredit ablösen', value: 3 },
    { label: 'Ausgleich Dispo', value: 2 },
  ];

  toggleDisableControlState(): void {
    this.control[this.control.disabled ? 'enable' : 'disable']();
  }
}
