export enum KeybordCodes {
  ARROW_UP = 38,
  ARROW_DOWN = 40,
  ESCAPE = 27,
  ENTER = 13
}
