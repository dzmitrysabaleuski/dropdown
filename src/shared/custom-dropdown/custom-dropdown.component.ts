import { Component, ElementRef, forwardRef, Input, OnDestroy, OnInit } from '@angular/core';
import { CustomDropdownOptionInterface } from '../interfaces/custom-dropdown-option';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { KeybordCodes } from '../enums/keybord-codes.enum';

@Component({
  selector: 'app-custom-dropdown',
  templateUrl: './custom-dropdown.component.html',
  styleUrls: ['./custom-dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomDropdownComponent),
      multi: true
    }
  ]
})
export class CustomDropdownComponent implements ControlValueAccessor, OnInit, OnDestroy {

  constructor(
    private elementRef: ElementRef
  ) {
  }

  get value(): string | number {
    return this.val;
  }

  set value(val: string | number) {
    this.val = val;
    this.setIndexBySelectedValue(val);
    this.onChange(val);
    this.onTouched();
  }

  get selectedLabel(): string {
    if (!this.value) {
      return this.options[0].label;
    }

    const selectedOption = this.getOptionByValue(this.value);
    return selectedOption ? selectedOption.label : this.options[0].label;
  }

  @Input() options: CustomDropdownOptionInterface[];
  @Input('value') val: string | number;

  disabled: boolean;
  isOpen: boolean;
  currentIndex: number;
  dropdownOpenedStatus$ = new Subject<boolean>();
  unsubscribe$ = new Subject();

  ngOnInit(): void {
    this.dropdownOpenedStatus$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(openStatus => {
        if (!openStatus) {
          return;
        }

        this.addClickListener();
        this.addBtnListener();
      });
  }

  // ControlValueAccessor methods
  onChange: any = () => {};
  onTouched: any = () => {};

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  writeValue(value: string | number) {
    if (!value) {
      return;
    }

    this.value = value;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  // Component logic
  addBtnListener(): void {
    fromEvent(document, 'keyup')
      .pipe(takeUntil(this.dropdownOpenedStatus$))
      .subscribe(this.keyboardEventsHandler.bind(this));
  }

  addClickListener(): void {
    fromEvent(document, 'click')
      .pipe(takeUntil(this.dropdownOpenedStatus$))
      .subscribe(event => {
        if (event.target === this.elementRef.nativeElement || this.elementRef.nativeElement.contains(event.target)) {
          return;
        }

        this.toggleDropdown();
      });
  }

  chooseOption(option: CustomDropdownOptionInterface) {
    this.value = option.value;
    this.toggleDropdown();
  }

  keyboardEventsHandler(event: KeyboardEvent) {
    switch (event.which) {
      case KeybordCodes.ARROW_UP:
        if (!this.currentIndex) {
          return;
        }

        this.currentIndex--;
        break;
      case KeybordCodes.ARROW_DOWN:
        if (this.options.length - 1 <= this.currentIndex) {
          return;
        }

        this.currentIndex++;
        break;
      case KeybordCodes.ESCAPE:
        this.toggleDropdown();
        break;
      case KeybordCodes.ENTER:
        this.value = this.options[this.currentIndex].value;
        this.toggleDropdown();
        break;
    }
  }

  getOptionByValue(value: string | number): CustomDropdownOptionInterface {
    return this.options.find(option => option.value === value);
  }

  getOptionIndexByValue(value: string | number): number {
    return this.options.findIndex(option => option.value === value);
  }

  isSelected(value: string | number, index: number): boolean {
    if (!this.currentIndex && this.currentIndex !== 0) {
      return value === this.value;
    }

    return this.currentIndex === index;
  }

  setIndexBySelectedValue(value: string | number): void {
    if (!value) {
      return;
    }

    const optionIndex = this.getOptionIndexByValue(value);

    if (!optionIndex) {
      return;
    }

    this.currentIndex = optionIndex;
  }

  toggleDropdown(): void {
    this.isOpen = !this.isOpen;
    this.dropdownOpenedStatus$.next(this.isOpen);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
  }
}
