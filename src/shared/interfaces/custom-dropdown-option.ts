export interface CustomDropdownOptionInterface {
  label: string;
  value: any;
  onClick: () => void;
}
